#FROM eclipse-temurin:17
#WORKDIR /application
#COPY . .
#RUN ./mvnw package
#ENTRYPOINT ["java" ,"-jar", "target/wordle-1.0-SNAPSHOT.jar"]
#mauvaise manière car on a le code source et les outils,
# il nous faut seulement le jar et de quoi le construire

#pour cela on utilie un multi-stage build

FROM maven:3.8.1-openjdk-17 AS builder
WORKDIR /app
COPY pom.xml .
RUN mvn -e -B dependency:resolve
COPY src ./src
RUN mvn clean -e -B package


FROM openjdk:slim
WORKDIR /app
COPY --from=builder /app/target/wordle-1.0-SNAPSHOT.jar .
ENTRYPOINT ["java", "-jar", "./wordle-1.0-SNAPSHOT.jar"]
